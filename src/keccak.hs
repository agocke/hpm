{-# LANGUAGE ForeignFunctionInterface #-}

module Keccak
  ( hash )
where

import Data.Int
import Data.ByteString.Char8
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String

data HashState

foreign import ccall unsafe "keccak/KeccakNISTInterface.h Init"
  c_Init :: Ptr HashState -> CInt -> IO CInt

foreign import ccall unsafe "keccak/KeccakNISTInterface.h Update"
  c_Update :: Ptr HashState -> Ptr CUChar -> CULLong -> IO CInt

foreign import ccall unsafe "keccak/KeccakNISTInterface.h Final"
  c_Final :: Ptr HashState -> Ptr CUChar -> IO CUInt

-- |Function to compute a hash using the Keccak[r, c] sponge function.
-- The rate r and capacity c values are determined from @a hashbitlen.
-- @return 
foreign import ccall unsafe "keccak/KeccakNISTInterface.h Hash"
  c_Hash :: CInt     -- ^ hashbitlen  The desired number of output bits.
         -> CString  -- ^ data        Pointer to the input data. 
         -> CInt     -- ^ databitLen  The number of input bits provided in the input data.
         -> CString  -- ^ hashval     Pointer to the buffer where to store the output data.
         -> IO CUInt -- ^ SUCCESS if successful, BAD_HASHLEN if the value of hashbitlen is incorrect.

-- |The number of bits in the hash output
output_bits = 512
-- |The number of bytes in the hash output
output_bytes = 64

-- |Compute the 512-bit hash of the given string
hash :: String -> IO ByteString
hash s = allocaBytes output_bytes with_outstring
  where with_outstring :: CString 
                       -> IO ByteString
        with_outstring out = withCStringLen s (call_c_Hash out)

        call_c_Hash :: CString
                    -> CStringLen
                    -> IO ByteString
        call_c_Hash out (cs, len) = do 
          let lenBits = fromIntegral (len * 8)::Int32
          _ <- c_Hash (CInt output_bits) cs (CInt lenBits) out
          packCStringLen (out, output_bytes)
