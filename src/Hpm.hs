
module Hpm ( build ) where

newtype FileSource = FileSource String

build :: Output -> IO ()
build exe = compile exe

cc :: FileSource -> (IO (), FileSource)
cc source = (gcc, output)
