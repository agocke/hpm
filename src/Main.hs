{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main )
where

import qualified Data.ByteString.Base64 as Base64
import qualified Data.ByteString.Char8 as ByteString
import qualified Keccak

-- import qualified Distribution.PackageDescription as PackageDescription
-- import Distribution.PackageDescription.Parse
--   ( readPackageDescription )
-- import Distribution.Simple.Configure
--   ( configure )
-- import Distribution.Simple.LocalBuildInfo
--   ( absoluteInstallDirs )
-- import Distribution.Simple.Setup
--   ( defaultConfigFlags )
-- import Distribution.Simple.Utils
--   ( defaultPackageDesc )
-- import qualified Distribution.Verbosity as Verbosity
-- import System.Environment ( getArgs )
-- 
-- main :: IO ()
-- main = do
--   description <- getPackageDescription
--   print description
-- 
-- installIfSimple :: PackageDescription.PackageDescription -> IO ()
-- installIfSimple pkgDesc
--   | Just PackageDescription.Simple <- PackageDescription.buildType pkgDesc
--   = putStrLn "A simple package"
-- installIfSimple _ = error "Not a simple package"
-- 
-- getPackageDescription :: IO PackageDescription.GenericPackageDescription
-- getPackageDescription = do
--   packageDescriptionFilePath <- defaultPackageDesc Verbosity.normal
--   description <- readPackageDescription Verbosity.normal packageDescriptionFilePath
--   return description
-- 
-- getInstallFiles :: PackageDescription.PackageDescription -> LocalBuildInfo -> CopyDest -> InstallDirs FilePath
-- getInstallFiles description buildInfo dest = do
--   dirs <- absoluteInstallDirs description buildInfo dest

main :: IO ()
main = do out <- Keccak.hash "something"
          ByteString.putStrLn $ Base64.encode out
