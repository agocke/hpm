import Hpm

main :: IO ()
main = Hpm.build exe

fileA_c = FileSource "fileA.c"

fileA_o = cc fileA_c

exe = clink fileA_o
